# Die Physik von Gravity - Realistisch wie behauptet?
Für viele ist es ein Traum, in das Weltall fliegen zu können aber leider haben
nicht alle Menschen die Möglichkeiten dazu 1. Weil Flüge ins Weltall mehrere
Millionen $ pro Flug kosten und 2. Weil die wenigen die geflogen werden hoch
trainiert sein müssen und spezifisch von Agenturen wie NASA ausgewählt werden
müssen. Im 2013 veröffentlichten Film Gravity z.B. bekamm die fiktive
Missionsspezialistin Dr. Ryan Stone die einzigartige Gelegenheit an board des
Space Shuttle ins Weltall zu fliegen, um Reparaturarbeiten am Weltraum Teleskop
Hubble durchzuführen. Es wird behauptet das dieser Film realistisch sei, aber
stimmt das? Haben sich die Beratungen mit Experten gelohnt? Ist dieser Film
wirklich Realistisch oder versteckt er, wie viele Filme, seine Fehler hinter
fortgeschrittener CGI?
