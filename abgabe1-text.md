### Zusammenfassung meiner bisherigen Ergebnisse
Hallo Herr Göttler,<br>
Im Anhang finden sie die erste Auffassung meiner Gliederung und Einleitung,
sowie eine Tabelle wo ich kurz auf alle Unterpunkte der Gliederung eingehe und
den Bezug zur Physik darstelle. Dazu gibt es auch die Quellen die ich bis jetzt
verwendet habe sowie Quellen die ich für das weiter Bearbeiten vorhab zu
verwenden. Ich hab in meine Gliederung einige Aspekte also hoffe ich das ich mir
selber nicht zu viel vorgenommen hab und nichts auslassen muss.

Für die weitere
Bearbeitung werde ich das Buch "Rocket Propulsion Elements" verwenden um über
die Formeln zu lernen die ich brauchen werde um die Antriebe zu vergleichen
(2.3). Da dieses Buch sich nur mit Raketenantriebe befasst werde ich andere
Quellen suchen müssen um mich über Orbitalmechanik zu informieren sodass ich 2.2
angehen kann.

2.2.1 ist einer der Aspekte zu den ich mich schon informiert habe
ich habe jedoch keine Quellen dazu gelistet da die Infos die ich bist jetzt habe
nur aus Vorwissen besteht und ich dazu noch nicht genaueres rescherchiert habe. Ich würde
für 2.2.1 über das Kessler Syndrom reden und welche Umlaufbahnen eine solche
Katastrophe beinflussen würde. Im Film wird ja angesprochen das ein russicher
Spionagesatellit von einer Anti Satellit Waffe zerstört wurde und das die daraus
resultierende Anhäufung an Weltraummüll eine Kettenreaktion an Kollisionen
ausgelösst hat was sogar zur Zerstörung von GPS und Kommunikations Satelliten
führte, da diese in verschiedenste Umlaufbahnen sich befinden würde ich rescherchieren
was für Simulationen schon gemacht wurden und was die Wahrscheinlichkeit einer
solchen Ausbreitung wäre. Zu Punkt 2.2.2 würde ich die Umlaufbahnen der
verschiedenen Stationen und des Shuttles vergleichen und berechnen wie viel
Energie man für die Transfer maneuver bräuchte im Vergleich zu was wirklich
vorhanden wäre um den Realismus der Situation zu prüfen und zu kritisieren. In
2.2.3 würde ich inbesondere auf besonders auf die chinesische Raumstation
eingehen da sie sogar schon am Ende des Films die Atmosphäre wiedereintritt.
Dieser Punkt könnte sich mit 2.4.2 überschneide also ist es möglich das ich die
beiden Aspkete in einen Punkt unterbringe.

Den ersten Aspket den ich in meiner Arbeit angehen werde ist den
unrealistischen/uneinheitlichen Sauerstoffkonsum von Dr. Stone dieser Punkt
wird als erster ausgewählt da der sich gut anbietet für eine Überleitung von
der Einleitung zum Hauptteil. 
