# Erste Quellen
* Netflix, der Film "Gravity"
* [Allgemeine Film infos][1] (Stand: 16.02.2020 18:55)
* Rocket Propulsion elements - 9th edition | George P. Sutton; Oscar Biblarz
* [Interview mit Regisseur von Gravity][2] (Stand: 23.03.2020 15:47)
* [Grundinformation zu NASA's EMU][3] (Stand: 26.03.2020 16:11)

[1]: https://en.wikipedia.org/wiki/Gravity_\(2013_film\)
[2]: https://www.youtube.com/watch?v=E8vDsW0D8hQ
[3]: https://en.wikipedia.org/wiki/Extravehicular_Mobility_Unit
