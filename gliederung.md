# Gliderung für "Die Physik von Gravity - Realistisch wie behauptet?"

## 1 Wie sterben Astronauten (Einleitung)
* Für viele ist es ihr Traumjob Astronaut zu werden
* Arbeitsthema vorstellen
* 1-2 Sätze über die Geschichte von Todesfälle in der Weltraumforschung
* Würde Dr. Ryan Stone (Protagonistin) realistisch überleben?

## 2 Physkalische Aspekte, Szenen & Analyse
  - ### 2.1 Lebenserhaltungssysteme & Biomechanik
    * 2.1.1 Sauerstoffmangel von Dr. Stone
    * 2.1.2 Wunde vom toten Astronaut (Loch im Kopf)
    * 2.1.3 Haluzinierte Öffnung der Soyuz und Druckentlastung
  - ### 2.2 Navigation, Umlauf- und Flugbahnen
    * 2.2.1 Umlaufbahn des Weltraummülls
    * 2.2.2 Navigation zwischen Hubble, ISS und der chinesischen Raumstation
    * 2.2.3 Flugbahn der chinesischen Raumstation
  - ### 2.3 Antriebe
    * 2.3.1 Feuerlöscher
    * 2.3.2 Raumanzug von Matt Kowalski
    * 2.3.3 Bremsantrieb von Soyuz
  - ### 2.4 Sonstiges
    * 2.4.1 Rotation des Spaceshuttles nach der Kollision
    * 2.4.2 Wiedereintrittsszene
## 3 RIP Dr. Stone? (Fazit)
* Rückbezug auf Anfangsfrage "Würde Dr. Stone überleben?" (Nein)
* Zusammenfassung ist Gravity wirklich realistisch
