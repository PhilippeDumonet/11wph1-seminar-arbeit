## Bezug der Arbeit "Die Physik von Gravity - Realistisch wie behauptet?" zur Physik
| Aspekt/Punkt | Bezug zur Physik / Analyse | Zeitabschnitt |
|:---:|:---:|:---:|
|Sauerstofmangel von Dr. Stone|Sauerstoff Verbrauch, Niveau und Verlauf|17:30-38:30|
|Wunde vom toten Astronaut (Loch im Kopf)|Ballistik, Kollisionsenergien und Wunde|12:10-13:00; 21:25|
|Haluzinierte Öffnung der Soyuz und Druckentlastung|Luftdruck und Luftschleuse|1:02:31-1:04:00|
|Umlaufbahn des Weltraummülls|Umlaufbahn (Orbitalmechanik)|Ganzer Film|
|Navigation zwischen Hubble, ISS und der chinesischen Raumstation|Umlaufbahnen, Flugbahnen und Geschichte|Ganzer Film|
|Flugbahn der chinesischen Raumstation|Umlaufbahn, Flugbahn und Wiedereintritt|Ganzer Film|
|Feuerlöscher Szene|Realismus Einschätzung durch Berechnung von Impuls und andere Antriebs Messwerte|1:11:10-1:12:15|
|Raumanzug von Matt Kowalski|Vergleich mit NASA's Extravehicular Mobility Unit, Berechnung und Vergleich von Impuls und andere Messwerte/Kennzahlen|2:00-31:00|
|Bremsantrieb von Soyuz (inkl. Szene)|Realismus Einschätzung durch Berechnung und Vergleich von Messwerte/Kennzahlen|1:09:50-1:10:15|
|Rotation des Spaceshuttles nach der Kollision|Berechnung der Kollision und enstehende Rotation|12:10-13:00|
|Wiedereintrittsszene|Flugbahn der Wiedereintrittsszene, Temperatur und Defekte der Kapsel|1:13:00-1:20:00|
